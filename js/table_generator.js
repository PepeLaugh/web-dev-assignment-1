"uses strict";
//event listeners to know when the table should be updated
window.addEventListener('DOMContentLoaded', function (){
    let rowCount = document.getElementById("row-count");
    rowCount.addEventListener("blur", generateTable);

    let colCount = document.getElementById("col-count");
    colCount.addEventListener("blur", generateTable);

    let tableWidth = document.getElementById("table-width");
    tableWidth.addEventListener("blur", generateTable);

    let textColor = document.getElementById("text-color");
    textColor.addEventListener("change", generateTable);

    let backgroundColor = document.getElementById("background-color");
    backgroundColor.addEventListener("change", generateTable);

    let borderWidth = document.getElementById("border-width");
    borderWidth.addEventListener("blur", generateTable);

    let borderColor = document.getElementById("border-color");
    borderColor.addEventListener("change", generateTable);
});

//get methods
function getRowCount(){
    let rowCount = document.getElementById("row-count").value;
    return rowCount;
}
function getColCount(){
    let colCount = document.getElementById("col-count").value;
    return colCount;
}
function getTableWidth(){
    let tableWidth = document.getElementById("table-width").value;
    return tableWidth;
}
function getTextColor(){
    let textColor = document.getElementById("text-color").value;
    return textColor;
}
function getBackgroundColor(){
    let backgroundColor = document.getElementById("background-color").value;
    return backgroundColor;
}
function getBorderWidth(){
    let borderWidth = document.getElementById("border-width").value;
    return borderWidth;
}
function getBorderColor(){
    let borderColor = document.getElementById("border-color").value;
    return borderColor;
}


//function that generates a table
function generateTable() {
    if(document.getElementById("table") != null){
        document.getElementById("table").remove();
    }

    let newTable = createElement("table", null, "table-render-space");
    newTable.id = "table";

    let rowCount = getRowCount();
    let colCount = getColCount();

    for(let i = 0; i < rowCount; i++){
        let tr = createElement("tr", null, "table");
        let id = "tr"+i;
        tr.id = id;

        for(let j = 0; j < colCount; j++){
            createElement("td", "cell"+i+j, id);
        }
    }
    changeTextColor("table", getTextColor());
    changeBackgroundColor("td", getBackgroundColor());
    changeTagWidth("table", getTableWidth());
    changeBorderColor("td", getBorderColor());
    changeBorderWidth("td", getBorderWidth());
    createHTMLText();
}


//function to represent the table
function createHTMLText(){
    if (document.querySelector("textarea") != null) {
        document.querySelector("textarea").remove();
    }
    createElement("textarea", null, "table-html-space");
    document.querySelector("textarea").readOnly=true;

    let text ="<table>\n";
    for(let i = 0; i < getRowCount(); i++){
        text += "\t<tr>\n";
        for(let j = 0; j < getColCount(); j++){
            text += "\t\t<td>cell" +i+j+"</td>\n";
        }
        text += "\t</tr>\n"
    }
    text += "</table>";
    document.querySelector("textarea").value = text;
}

