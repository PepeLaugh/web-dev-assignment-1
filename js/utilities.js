"uses strict";
/**
 * creates an element.
 * @constructor
 * @param {string} tag - The tag.
 * @param {string} content - The text content.
 * @param {string} parent - The parent.
 */
function createElement(tag, content, parent){
    let node = document.createElement(tag);
    node.textContent=content;
    let prnt = document.getElementById(parent);
    prnt.append(node);
    return node;
}
/**
 * changes the color of text.
 * @constructor
 * @param {string} tag - The tag.
 * @param {string} color - The color.
 */
function changeTextColor(tag, color){
    let elem = document.querySelectorAll(tag);
    for(let i=0; i<elem.length; i++){
        elem[i].style.color=color;
    }
}
/**
 * changes the color of background.
 * @constructor
 * @param {string} tag - The tag.
 * @param {string} color - The color.
 */
function changeBackgroundColor(tag, color){
    let elem = document.querySelectorAll(tag);
    for(let i=0; i<elem.length; i++){
        elem[i].style.backgroundColor=color;
    }
}
/**
 * changes tag's width.
 * @constructor
 * @param {string} tag - The tag.
 * @param {string} percent - The percentage.
 */
function changeTagWidth(tag, percent){
    let elem = document.querySelectorAll(tag);
    for(let i=0; i<elem.length; i++){
        elem[i].style.width=percent + "%";
    }
}
/**
 * changes color of border.
 * @constructor
 * @param {string} tag - The tag.
 * @param {string} color - The color.
 */
function changeBorderColor(tag, color){
    let elem = document.querySelectorAll(tag);
    for(let i=0; i<elem.length; i++){
        elem[i].style.borderColor=color;
    }
}
/**
 * changes width of border.
 * @constructor
 * @param {string} tag - The tag.
 * @param {string} pixels - The number of pixels.
 */
function changeBorderWidth(tag, pixels){
    let elem = document.querySelectorAll(tag);
    for(let i=0; i<elem.length; i++){
        elem[i].style.borderWidth=pixels + "px";
    }
}

